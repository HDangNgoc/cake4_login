<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MStudent $mStudent
 * @var \Cake\Collection\CollectionInterface|string[] $parentMStudents
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List M Students'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mStudents form content">
            <?= $this->Form->create($mStudent) ?>
            <fieldset>
                <legend><?= __('Add M Student') ?></legend>
                <?php
                    echo $this->Form->control('student_no');
                    echo $this->Form->control('student_name');
                    echo $this->Form->control('student_name_phonetic');
                    echo $this->Form->control('student_birthday', ['empty' => true]);
                    echo $this->Form->control('enrollment_status');
                    echo $this->Form->control('academy_name');
                    echo $this->Form->control('course_name');
                    echo $this->Form->control('department_name');
                    echo $this->Form->control('grade');
                    echo $this->Form->control('class');
                    echo $this->Form->control('tuition_billing_destination');
                    echo $this->Form->control('student_postcode');
                    echo $this->Form->control('student_prefecture');
                    echo $this->Form->control('student_address');
                    echo $this->Form->control('student_tel');
                    echo $this->Form->control('student_email');
                    echo $this->Form->control('parent_name');
                    echo $this->Form->control('parent_postcode');
                    echo $this->Form->control('parent_prefecture');
                    echo $this->Form->control('parent_address');
                    echo $this->Form->control('parent_tel');
                    echo $this->Form->control('parent_email');
                    echo $this->Form->control('student_password');
                    echo $this->Form->control('parent_id', ['options' => $parentMStudents, 'empty' => true]);
                    echo $this->Form->control('parent_password');
                    echo $this->Form->control('is_deleted');
                    echo $this->Form->control('created_by');
                    echo $this->Form->control('created_at', ['empty' => true]);
                    echo $this->Form->control('updated_by');
                    echo $this->Form->control('updated_at', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
