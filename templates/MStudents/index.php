<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MStudent[]|\Cake\Collection\CollectionInterface $mStudents
 */
?>
<div class="mStudents index content">
    <?= $this->Html->link(__('New M Student'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('M Students') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('student_no') ?></th>
                    <th><?= $this->Paginator->sort('student_name') ?></th>
                    <th><?= $this->Paginator->sort('student_name_phonetic') ?></th>
                    <th><?= $this->Paginator->sort('student_birthday') ?></th>
                    <th><?= $this->Paginator->sort('enrollment_status') ?></th>
                    <th><?= $this->Paginator->sort('academy_name') ?></th>
                    <th><?= $this->Paginator->sort('course_name') ?></th>
                    <th><?= $this->Paginator->sort('department_name') ?></th>
                    <th><?= $this->Paginator->sort('grade') ?></th>
                    <th><?= $this->Paginator->sort('class') ?></th>
                    <th><?= $this->Paginator->sort('tuition_billing_destination') ?></th>
                    <th><?= $this->Paginator->sort('student_postcode') ?></th>
                    <th><?= $this->Paginator->sort('student_prefecture') ?></th>
                    <th><?= $this->Paginator->sort('student_address') ?></th>
                    <th><?= $this->Paginator->sort('student_tel') ?></th>
                    <th><?= $this->Paginator->sort('student_email') ?></th>
                    <th><?= $this->Paginator->sort('parent_name') ?></th>
                    <th><?= $this->Paginator->sort('parent_postcode') ?></th>
                    <th><?= $this->Paginator->sort('parent_prefecture') ?></th>
                    <th><?= $this->Paginator->sort('parent_address') ?></th>
                    <th><?= $this->Paginator->sort('parent_tel') ?></th>
                    <th><?= $this->Paginator->sort('parent_email') ?></th>
                    <th><?= $this->Paginator->sort('student_password') ?></th>
                    <th><?= $this->Paginator->sort('parent_id') ?></th>
                    <th><?= $this->Paginator->sort('parent_password') ?></th>
                    <th><?= $this->Paginator->sort('is_deleted') ?></th>
                    <th><?= $this->Paginator->sort('created_by') ?></th>
                    <th><?= $this->Paginator->sort('created_at') ?></th>
                    <th><?= $this->Paginator->sort('updated_by') ?></th>
                    <th><?= $this->Paginator->sort('updated_at') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($mStudents as $mStudent): ?>
                <tr>
                    <td><?= $this->Number->format($mStudent->id) ?></td>
                    <td><?= h($mStudent->student_no) ?></td>
                    <td><?= h($mStudent->student_name) ?></td>
                    <td><?= h($mStudent->student_name_phonetic) ?></td>
                    <td><?= h($mStudent->student_birthday) ?></td>
                    <td><?= h($mStudent->enrollment_status) ?></td>
                    <td><?= h($mStudent->academy_name) ?></td>
                    <td><?= h($mStudent->course_name) ?></td>
                    <td><?= h($mStudent->department_name) ?></td>
                    <td><?= $this->Number->format($mStudent->grade) ?></td>
                    <td><?= h($mStudent->class) ?></td>
                    <td><?= h($mStudent->tuition_billing_destination) ?></td>
                    <td><?= h($mStudent->student_postcode) ?></td>
                    <td><?= h($mStudent->student_prefecture) ?></td>
                    <td><?= h($mStudent->student_address) ?></td>
                    <td><?= h($mStudent->student_tel) ?></td>
                    <td><?= h($mStudent->student_email) ?></td>
                    <td><?= h($mStudent->parent_name) ?></td>
                    <td><?= h($mStudent->parent_postcode) ?></td>
                    <td><?= h($mStudent->parent_prefecture) ?></td>
                    <td><?= h($mStudent->parent_address) ?></td>
                    <td><?= h($mStudent->parent_tel) ?></td>
                    <td><?= h($mStudent->parent_email) ?></td>
                    <td><?= h($mStudent->student_password) ?></td>
                    <td><?= $mStudent->has('parent_m_student') ? $this->Html->link($mStudent->parent_m_student->id, ['controller' => 'MStudents', 'action' => 'view', $mStudent->parent_m_student->id]) : '' ?></td>
                    <td><?= h($mStudent->parent_password) ?></td>
                    <td><?= h($mStudent->is_deleted) ?></td>
                    <td><?= h($mStudent->created_by) ?></td>
                    <td><?= h($mStudent->created_at) ?></td>
                    <td><?= h($mStudent->updated_by) ?></td>
                    <td><?= h($mStudent->updated_at) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $mStudent->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mStudent->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mStudent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mStudent->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
