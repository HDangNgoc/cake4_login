<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MAdmin $mAdmin
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List M Admins'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mAdmins form content">
            <?= $this->Form->create($mAdmin) ?>
            <fieldset>
                <legend><?= __('Add M Admin') ?></legend>
                <?php
                    echo $this->Form->control('admin_login_id');
                    echo $this->Form->control('admin_name');
                    echo $this->Form->control('admin_password');
                    echo $this->Form->input('academy_id', array('type'=>'hidden', 'value'=>'01'));
                    echo $this->form->label('Admin Role');
                    echo $this->Form->select('admin_role',
                        ['管理者','担当者'],
                        ['empty' => '(choose one)']
                    );
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
