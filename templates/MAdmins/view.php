<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MAdmin $mAdmin
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit M Admin'), ['action' => 'edit', $mAdmin->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete M Admin'), ['action' => 'delete', $mAdmin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mAdmin->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List M Admins'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New M Admin'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mAdmins view content">
            <h3><?= h($mAdmin->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Admin Login Id') ?></th>
                    <td><?= h($mAdmin->admin_login_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Admin Name') ?></th>
                    <td><?= h($mAdmin->admin_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Admin Password') ?></th>
                    <td><?= h($mAdmin->admin_password) ?></td>
                </tr>
                <tr>
                    <th><?= __('Academy Id') ?></th>
                    <td><?= h($mAdmin->academy_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created By') ?></th>
                    <td><?= h($mAdmin->created_by) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated By') ?></th>
                    <td><?= h($mAdmin->updated_by) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($mAdmin->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Admin Role') ?></th>
                    <td><?= $this->Number->format($mAdmin->admin_role) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created At') ?></th>
                    <td><?= h($mAdmin->created_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated At') ?></th>
                    <td><?= h($mAdmin->updated_at) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Deleted') ?></th>
                    <td><?= $mAdmin->is_deleted ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
