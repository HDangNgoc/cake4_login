<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MAdmin[]|\Cake\Collection\CollectionInterface $mAdmins
 */
?>
<div class="mAdmins index content">
    <?= $this->Html->link(__('New M Admin'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('M Admins') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('admin_login_id') ?></th>
                    <th><?= $this->Paginator->sort('admin_name') ?></th>
                    <th><?= $this->Paginator->sort('admin_password') ?></th>
                    <th><?= $this->Paginator->sort('academy_id') ?></th>
                    <th><?= $this->Paginator->sort('admin_role') ?></th>
                    <th><?= $this->Paginator->sort('is_deleted') ?></th>
                    <th><?= $this->Paginator->sort('created_by') ?></th>
                    <th><?= $this->Paginator->sort('created_at') ?></th>
                    <th><?= $this->Paginator->sort('updated_by') ?></th>
                    <th><?= $this->Paginator->sort('updated_at') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($mAdmins as $mAdmin): ?>
                <tr>
                    <td><?= $this->Number->format($mAdmin->id) ?></td>
                    <td><?= h($mAdmin->admin_login_id) ?></td>
                    <td><?= h($mAdmin->admin_name) ?></td>
                    <td><?= h($mAdmin->admin_password) ?></td>
                    <td><?= h($mAdmin->academy_id) ?></td>
                    <td><?= $this->Number->format($mAdmin->admin_role) ?></td>
                    <td><?= h($mAdmin->is_deleted) ?></td>
                    <td><?= h($mAdmin->created_by) ?></td>
                    <td><?= h($mAdmin->created_at) ?></td>
                    <td><?= h($mAdmin->updated_by) ?></td>
                    <td><?= h($mAdmin->updated_at) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $mAdmin->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mAdmin->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mAdmin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mAdmin->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
