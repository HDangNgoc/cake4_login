<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * MStudents Controller
 *
 * @property \App\Model\Table\MStudentsTable $MStudents
 * @method \App\Model\Entity\MStudent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MStudentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentMStudents'],
        ];
        $mStudents = $this->paginate($this->MStudents);

        $this->set(compact('mStudents'));
    }

    /**
     * View method
     *
     * @param string|null $id M Student id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mStudent = $this->MStudents->get($id, [
            'contain' => ['ParentMStudents', 'ChildMStudents'],
        ]);

        $this->set(compact('mStudent'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mStudent = $this->MStudents->newEmptyEntity();
        if ($this->request->is('post')) {
            $mStudent = $this->MStudents->patchEntity($mStudent, $this->request->getData());
            if ($this->MStudents->save($mStudent)) {
                $this->Flash->success(__('The m student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The m student could not be saved. Please, try again.'));
        }
        $parentMStudents = $this->MStudents->ParentMStudents->find('list', ['limit' => 200]);
        $this->set(compact('mStudent', 'parentMStudents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id M Student id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mStudent = $this->MStudents->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mStudent = $this->MStudents->patchEntity($mStudent, $this->request->getData());
            if ($this->MStudents->save($mStudent)) {
                $this->Flash->success(__('The m student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The m student could not be saved. Please, try again.'));
        }
        $parentMStudents = $this->MStudents->ParentMStudents->find('list', ['limit' => 200]);
        $this->set(compact('mStudent', 'parentMStudents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id M Student id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mStudent = $this->MStudents->get($id);
        if ($this->MStudents->delete($mStudent)) {
            $this->Flash->success(__('The m student has been deleted.'));
        } else {
            $this->Flash->error(__('The m student could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
