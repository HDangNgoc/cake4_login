<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * MAdmins Controller
 *
 * @property \App\Model\Table\MAdminsTable $MAdmins
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MAdminsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->loadModel('MAdmins');
        $mAdmins = $this->paginate($this->MAdmins);

        $this->set(compact('mAdmins'));
    }

    /**
     * View method
     *
     * @param string|null $id M Admin id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mAdmin = $this->MAdmins->get($id, [
            'contain' => ['AdminLogins', 'Academies'],
        ]);

        $this->set(compact('mAdmin'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {   $this->loadModel('MAdmins');
        $mAdmin = $this->MAdmins->newEmptyEntity();
        if ($this->request->is('post')) {
            $mAdmin = $this->MAdmins->patchEntity($mAdmin, $this->request->getData());
            if ($this->MAdmins->save($mAdmin)) {
                $this->Flash->success(__('The m admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The m admin could not be saved. Please, try again.'));
        }
        $this->set(compact('mAdmin', 'adminLogins', 'academies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id M Admin id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mAdmin = $this->MAdmins->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mAdmin = $this->MAdmins->patchEntity($mAdmin, $this->request->getData());
            if ($this->MAdmins->save($mAdmin)) {
                $this->Flash->success(__('The m admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The m admin could not be saved. Please, try again.'));
        }
        $adminLogins = $this->MAdmins->AdminLogins->find('list', ['limit' => 200]);
        $academies = $this->MAdmins->Academies->find('list', ['limit' => 200]);
        $this->set(compact('mAdmin', 'adminLogins', 'academies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id M Admin id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mAdmin = $this->MAdmins->get($id);
        if ($this->MAdmins->delete($mAdmin)) {
            $this->Flash->success(__('The m admin has been deleted.'));
        } else {
            $this->Flash->error(__('The m admin could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login', 'index']);
    }

    public function login()
    {
    $result = $this->Authentication->getResult();
    if ($result->isValid()) {
        $redirect = $this->request->getQuery('redirect', [
            'controller' => 'MAdmins',
            'action' => 'index',
        ]);   
            
        return $this->redirect($redirect);
    }
    if ($this->request->is('post') && !$result->isValid()) {
        $this->Flash->error(__('ユーザー名またはパスワードが誤っています。'));
    }
    }

    public function logout()
    {
    $result = $this->Authentication->getResult();
    if ($result->isValid()) {
        $this->Authentication->logout();
    }
    return $this->redirect(['controller' => 'MAdmins', 'action' => 'login']);
    }
}
