<?php
declare(strict_types=1);

namespace App\Controller;


class UsersController extends AppController
{   public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
    }

    public function login()
    {
    $result = $this->Authentication->getResult();
    if ($result->isValid()) {
        $redirect = $this->request->getQuery('redirect', [
            'controller' => 'Users',
            'action' => 'index',
        ]);   
            
        return $this->redirect($redirect);
    }
    if ($this->request->is('post') && !$result->isValid()) {
        $this->Flash->error(__('ユーザー名またはパスワードが誤っています。'));
    }
    }

    public function logout()
    {
    $result = $this->Authentication->getResult();
    if ($result->isValid()) {
        $this->Authentication->logout();
    }
    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }
    
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
    }

    
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function test()
    {
        $this->loadModel('users');
        $users = $this->paginate($this->users);
        $this->set(compact('users'));
    }
    
    public function info()
    {   
        $this->loadModel('users');
        $id=$this->request->getquery('keyword');
        $this->set('id',$id);
        
    }

    public function update()
    {   $this->viewBuilder()->setLayout('ajax');
        $this->loadModel('users');
        $data_request = $this->request->getQuery('request');
        $data_id = $this->request->getQuery('id');
        $data=array();
        $datas=array('id'=>$data_id, 'username'=>$data_request);
        $entity = $this->Users->NewEntity($data);
        $query = $this->Users->query('SELECT id from users WHERE id = '.$data_id.'');
    if(isset($query))
    {
        $data=$datas;
        $result = $this->Users->patchEntity($entity,$data); 

    };
        $this->Users->save($result);
        print_r($result);
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
