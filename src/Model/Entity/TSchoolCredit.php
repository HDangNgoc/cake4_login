<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TSchoolCredit Entity
 *
 * @property int $student_no
 * @property int $grade
 * @property int $graduation_credits
 * @property float $attendance_rate
 */
class TSchoolCredit extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'grade' => true,
        'graduation_credits' => true,
        'attendance_rate' => true,
    ];
}
