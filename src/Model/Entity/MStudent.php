<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MStudent Entity
 *
 * @property int $id
 * @property string|null $student_no
 * @property string $student_name
 * @property string|null $student_name_phonetic
 * @property \Cake\I18n\FrozenTime|null $student_birthday
 * @property string $enrollment_status
 * @property string $academy_name
 * @property string $course_name
 * @property string $department_name
 * @property int $grade
 * @property string $class
 * @property string $tuition_billing_destination
 * @property string|null $student_postcode
 * @property string|null $student_prefecture
 * @property string|null $student_address
 * @property string|null $student_tel
 * @property string|null $student_email
 * @property string|null $parent_name
 * @property string|null $parent_postcode
 * @property string|null $parent_prefecture
 * @property string|null $parent_address
 * @property string|null $parent_tel
 * @property string|null $parent_email
 * @property string $student_password
 * @property string|null $parent_id
 * @property string $parent_password
 * @property bool $is_deleted
 * @property string|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property string|null $updated_by
 * @property \Cake\I18n\FrozenTime|null $updated_at
 *
 * @property \App\Model\Entity\ParentMStudent $parent_m_student
 * @property \App\Model\Entity\ChildMStudent[] $child_m_students
 */
class MStudent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'student_no' => true,
        'student_name' => true,
        'student_name_phonetic' => true,
        'student_birthday' => true,
        'enrollment_status' => true,
        'academy_name' => true,
        'course_name' => true,
        'department_name' => true,
        'grade' => true,
        'class' => true,
        'tuition_billing_destination' => true,
        'student_postcode' => true,
        'student_prefecture' => true,
        'student_address' => true,
        'student_tel' => true,
        'student_email' => true,
        'parent_name' => true,
        'parent_postcode' => true,
        'parent_prefecture' => true,
        'parent_address' => true,
        'parent_tel' => true,
        'parent_email' => true,
        'student_password' => true,
        'parent_id' => true,
        'parent_password' => true,
        'is_deleted' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
        'parent_m_student' => true,
        'child_m_students' => true,
    ];
}
