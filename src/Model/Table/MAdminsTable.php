<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MAdmins Model
 *
 * @property \App\Model\Table\AdminLoginsTable&\Cake\ORM\Association\BelongsTo $AdminLogins
 * @property \App\Model\Table\AcademiesTable&\Cake\ORM\Association\BelongsTo $Academies
 *
 * @method \App\Model\Entity\MAdmin newEmptyEntity()
 * @method \App\Model\Entity\MAdmin newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin get($primaryKey, $options = [])
 * @method \App\Model\Entity\MAdmin findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\MAdmin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\MAdmin|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MAdmin saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MAdmin[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MAdminsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('m_admins');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AdminLogins', [
            'foreignKey' => 'admin_login_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Academies', [
            'foreignKey' => 'academy_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('admin_name')
            ->maxLength('admin_name', 50)
            ->requirePresence('admin_name', 'create')
            ->notEmptyString('admin_name');

        $validator
            ->scalar('admin_password')
            ->maxLength('admin_password', 255)
            ->requirePresence('admin_password', 'create')
            ->notEmptyString('admin_password');

        $validator
            ->requirePresence('admin_role', 'create')
            ->notEmptyString('admin_role');

        $validator
            ->boolean('is_deleted')
            ->notEmptyString('is_deleted');

        $validator
            ->scalar('created_by')
            ->maxLength('created_by', 50)
            ->allowEmptyString('created_by');

        $validator
            ->dateTime('created_at')
            ->allowEmptyDateTime('created_at');

        $validator
            ->scalar('updated_by')
            ->maxLength('updated_by', 50)
            ->allowEmptyString('updated_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmptyDateTime('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['admin_login_id'], 'AdminLogins'), ['errorField' => 'admin_login_id']);
        $rules->add($rules->existsIn(['academy_id'], 'Academies'), ['errorField' => 'academy_id']);

        return $rules;
    }
}
