<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TSchoolCredit Model
 *
 * @method \App\Model\Entity\TSchoolCredit newEmptyEntity()
 * @method \App\Model\Entity\TSchoolCredit newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\TSchoolCredit[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TSchoolCredit get($primaryKey, $options = [])
 * @method \App\Model\Entity\TSchoolCredit findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\TSchoolCredit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TSchoolCredit[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\TSchoolCredit|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TSchoolCredit saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TSchoolCredit[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TSchoolCredit[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\TSchoolCredit[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TSchoolCredit[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class TSchoolCreditTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('t_school_credit');
        $this->setDisplayField('student_no');
        $this->setPrimaryKey('student_no');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('student_no')
            ->allowEmptyString('student_no', null, 'create');

        $validator
            ->requirePresence('grade', 'create')
            ->notEmptyString('grade');

        $validator
            ->requirePresence('graduation_credits', 'create')
            ->notEmptyString('graduation_credits');

        $validator
            ->numeric('attendance_rate')
            ->requirePresence('attendance_rate', 'create')
            ->notEmptyString('attendance_rate');

        return $validator;
    }
}
