<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MStudents Model
 *
 * @property \App\Model\Table\MStudentsTable&\Cake\ORM\Association\BelongsTo $ParentMStudents
 * @property \App\Model\Table\MStudentsTable&\Cake\ORM\Association\HasMany $ChildMStudents
 *
 * @method \App\Model\Entity\MStudent newEmptyEntity()
 * @method \App\Model\Entity\MStudent newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\MStudent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MStudent get($primaryKey, $options = [])
 * @method \App\Model\Entity\MStudent findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\MStudent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MStudent[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\MStudent|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MStudent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MStudent[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MStudent[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\MStudent[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\MStudent[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MStudentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('m_students');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ParentMStudents', [
            'className' => 'MStudents',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('ChildMStudents', [
            'className' => 'MStudents',
            'foreignKey' => 'parent_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('student_no')
            ->maxLength('student_no', 20)
            ->allowEmptyString('student_no')
            ->add('student_no', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('student_name')
            ->maxLength('student_name', 100)
            ->requirePresence('student_name', 'create')
            ->notEmptyString('student_name');

        $validator
            ->scalar('student_name_phonetic')
            ->maxLength('student_name_phonetic', 100)
            ->allowEmptyString('student_name_phonetic');

        $validator
            ->dateTime('student_birthday')
            ->allowEmptyDateTime('student_birthday');

        $validator
            ->scalar('enrollment_status')
            ->maxLength('enrollment_status', 6)
            ->requirePresence('enrollment_status', 'create')
            ->notEmptyString('enrollment_status');

        $validator
            ->scalar('academy_name')
            ->maxLength('academy_name', 100)
            ->requirePresence('academy_name', 'create')
            ->notEmptyString('academy_name');

        $validator
            ->scalar('course_name')
            ->maxLength('course_name', 250)
            ->requirePresence('course_name', 'create')
            ->notEmptyString('course_name');

        $validator
            ->scalar('department_name')
            ->maxLength('department_name', 100)
            ->requirePresence('department_name', 'create')
            ->notEmptyString('department_name');

        $validator
            ->requirePresence('grade', 'create')
            ->notEmptyString('grade');

        $validator
            ->scalar('class')
            ->maxLength('class', 20)
            ->requirePresence('class', 'create')
            ->notEmptyString('class');

        $validator
            ->scalar('tuition_billing_destination')
            ->maxLength('tuition_billing_destination', 20)
            ->requirePresence('tuition_billing_destination', 'create')
            ->notEmptyString('tuition_billing_destination');

        $validator
            ->scalar('student_postcode')
            ->maxLength('student_postcode', 7)
            ->allowEmptyString('student_postcode');

        $validator
            ->scalar('student_prefecture')
            ->maxLength('student_prefecture', 10)
            ->allowEmptyString('student_prefecture');

        $validator
            ->scalar('student_address')
            ->maxLength('student_address', 120)
            ->allowEmptyString('student_address');

        $validator
            ->scalar('student_tel')
            ->maxLength('student_tel', 20)
            ->allowEmptyString('student_tel');

        $validator
            ->scalar('student_email')
            ->maxLength('student_email', 100)
            ->allowEmptyString('student_email');

        $validator
            ->scalar('parent_name')
            ->maxLength('parent_name', 100)
            ->allowEmptyString('parent_name');

        $validator
            ->scalar('parent_postcode')
            ->maxLength('parent_postcode', 7)
            ->allowEmptyString('parent_postcode');

        $validator
            ->scalar('parent_prefecture')
            ->maxLength('parent_prefecture', 50)
            ->allowEmptyString('parent_prefecture');

        $validator
            ->scalar('parent_address')
            ->maxLength('parent_address', 120)
            ->allowEmptyString('parent_address');

        $validator
            ->scalar('parent_tel')
            ->maxLength('parent_tel', 20)
            ->allowEmptyString('parent_tel');

        $validator
            ->scalar('parent_email')
            ->maxLength('parent_email', 100)
            ->allowEmptyString('parent_email');

        $validator
            ->scalar('student_password')
            ->maxLength('student_password', 255)
            ->requirePresence('student_password', 'create')
            ->notEmptyString('student_password');

        $validator
            ->scalar('parent_password')
            ->maxLength('parent_password', 255)
            ->requirePresence('parent_password', 'create')
            ->notEmptyString('parent_password');

        $validator
            ->boolean('is_deleted')
            ->notEmptyString('is_deleted');

        $validator
            ->scalar('created_by')
            ->maxLength('created_by', 50)
            ->allowEmptyString('created_by');

        $validator
            ->dateTime('created_at')
            ->allowEmptyDateTime('created_at');

        $validator
            ->scalar('updated_by')
            ->maxLength('updated_by', 50)
            ->allowEmptyString('updated_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmptyDateTime('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['student_no']), ['errorField' => 'student_no']);
        $rules->add($rules->existsIn(['parent_id'], 'ParentMStudents'), ['errorField' => 'parent_id']);

        return $rules;
    }
}
