<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TSchoolCreditTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TSchoolCreditTable Test Case
 */
class TSchoolCreditTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TSchoolCreditTable
     */
    protected $TSchoolCredit;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.TSchoolCredit',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('TSchoolCredit') ? [] : ['className' => TSchoolCreditTable::class];
        $this->TSchoolCredit = $this->getTableLocator()->get('TSchoolCredit', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TSchoolCredit);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\TSchoolCreditTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
