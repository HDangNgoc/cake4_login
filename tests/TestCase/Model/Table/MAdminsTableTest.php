<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MAdminsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MAdminsTable Test Case
 */
class MAdminsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MAdminsTable
     */
    protected $MAdmins;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MAdmins',
        'app.AdminLogins',
        'app.Academies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MAdmins') ? [] : ['className' => MAdminsTable::class];
        $this->MAdmins = $this->getTableLocator()->get('MAdmins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MAdmins);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MAdminsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\MAdminsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
