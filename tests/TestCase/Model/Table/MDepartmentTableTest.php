<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MDepartmentTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MDepartmentTable Test Case
 */
class MDepartmentTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MDepartmentTable
     */
    protected $MDepartment;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MDepartment',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MDepartment') ? [] : ['className' => MDepartmentTable::class];
        $this->MDepartment = $this->getTableLocator()->get('MDepartment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MDepartment);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MDepartmentTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
